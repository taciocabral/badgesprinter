from tkinter import *

class Application(object):
    def __init__(self, master=None):
        # Janela Top-level/Master
        master.minsize(width=800, height=600)
        master.resizable(width=False, height=False)
        master.title('Badge Printer')

        # TopMenu
        menubar = Menu(master)
        master.config(menu=menubar)

        geralMenu = Menu(menubar, tearoff=0)
        geralMenu.add_command(label='Sair', command=quit)
        menubar.add_cascade(label='Preferências', menu=geralMenu)
