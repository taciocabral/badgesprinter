from database.settings import Database


class UsuariosHandlers:
    def __init__(self, idusuario=0, nome="", telefone="", email="", usuario="", senha=""):
        self.info = {}
        self.idusuario = idusuario
        self.nome = nome
        self.telefone = telefone
        self.email = email
        self.usuario = usuario
        self.senha = senha

    def insert_usuario(self):

        banco = Database()
        
        try:
            c = banco.conexao.cursor()
            
            c.execute(
                "INSERT INTO tb_usuarios (nome, telefone, email, usuario, senha) VALUES (?, ?, ?, ?, ?)", (self.nome, self.telefone, self.email, self.usuario, self.senha)
            )

            banco.conexao.commit()
            c.close()

            return "Usuário cadastrado com sucesso!"

        except:
            return "Ocorreu um erro na inserção do usuário"
    
    def update_usuario(self):
        
        banco = Database()

        try:
            c = banco.conexao.cursor()

            c.execute(
                "UPDATE tb_usuarios SET nome = %s, telefone = %s, email = %s, usuario = %s, senha = %s where idusuario = %s" % (self.nome, self.telefone, self.email, self.usuario, self.senha, self.idusuario)
            )

            banco.conexao.commit()
            c.close()

            return "Usuário atualizado com sucesso!"
        
        except:
            return "Ocorreu um erro na alteração do usuário"
    
    def delete_usuario(self):

        banco = Database()

        try:
            c = banco.conexao.cursor()

            c.execute("DELETE FROM tb_usuarios WHERE id_usuario = %s;" % self.idusuario)

            banco.conexao.commit()

            c.close()

            return "Usuário excluído com sucesso!"
        
        except:
            return "Ocorreu um erro ao tentar excluir usuário"
    
    def select_usuario(self):

        banco = Database()

        try:
            c = banco.conexao.cursor()

            c.execute("SELECT * FROM tb_usuarios where idusuario = %s;" % self.idusuario )

            for linha in c:
                self.idusuario = linha[0]
                self.nome = linha[1]
                self.telefone = linha[2]
                self.email = linha[3]
                self.usuario = linha[4]
                self.senha = linha[5]
        
            c.close()

            return "Busca completa"
        
        except:
            return "Ocorreu um erro na busca pelo usuário"
