import sqlite3


class Database():

    def __init__(self):
        self.conexao = sqlite3.connect('BadgesPrinter.db')
        self.createTable()

    def createTable(self):
        c = self.conexao.cursor() # CURSOR é um interador que permite manipular os registros do DB

        # EXECUTE lê e executa comandos SQL puro diretamente no DB
        c.execute(
            """
                CREATE TABLE if not exists tb_usuarios (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                nome TEXT NOT NULL,
                telefone VARCHAR(11),
                email TEXT NOT NULL,
                usuario TEXT NOT NULL,
                senha TEXT NOT NULL,
                criado_em DATE DEFAULT CURRENT_DATE
            );
            """
        )

        print("Tabela criada com sucesso")
        self.conexao.commit()

        # Desconectando...
        c.close()
