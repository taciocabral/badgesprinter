from tkinter import *
from application import Application


class GuiLogin(Application):
    def __init__(self, master=None):
        super().__init__(top=master)

        self.container1 = Frame(master)
        self.container1.pack()
        self.container2 = Frame(master)
        self.container2['pady'] = 10
        self.container2.pack()
        self.container3 = Frame(master)
        self.container3.pack()

        self.login = Label(self.container1, text="Login:", font="Verdana 12 bold")
        self.login_entry = Entry(self.container1)
        self.login.pack(side=LEFT)
        self.login_entry.pack(side=LEFT)

        self.password = Label(self.container2, text="Senha:", font="Verdana 12 bold")
        self.password_entry = Entry(self.container2, show="•")
        self.password.pack(side=LEFT)
        self.password_entry.pack(side=LEFT)

        self.autenticar = Button(self.container3, text="Autenticar", command=self.authenticate)
        self.autenticar.pack(side=LEFT)

    def authenticate(self):
        pass

login = Tk()
GuiLogin(login)
login.mainloop()