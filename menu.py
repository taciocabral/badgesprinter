from tkinter import *
from application import Application

from localidades import gui as lcg

class MenuGui(Application):
    def __init__(self, master=None):
        super().__init__(master=master)

        # Containers
        self.container1 = Frame(master)
        self.container1.pack()
        self.container2 = Frame(master)
        self.container2['pady'] = 30
        self.container2['padx'] = 15
        self.container2.pack()
        self.container3 = Frame(master)
        self.container3['pady'] = 30
        self.container3['padx'] = 15
        self.container3.pack()

        # Menu - cadastro
        self.cadastro_image = PhotoImage(file='assets/img/cadastro.png')
        self.opcao_cadastro = Button(
                self.container2,
                compound=TOP,
                width=50, height=50,
                image=self.cadastro_image,
                text='Cadastro',
                borderwidth=0,
                highlightthickness=0
            )
        self.opcao_cadastro.pack(
                side=LEFT,
                padx=100,
                pady=20
            )

        # Menu - localidades
        self.opcao_localidades = Button(
                self.container2,
                compound=TOP,
                width=50, height=50,
                image=PhotoImage(file='assets/img/maps.png'),
                text='Localidades'
            )
        self.opcao_localidades.pack(
                side=LEFT,
                padx=100,
                pady=20
            )

        # Menu - irmãos
        self.irmaos_image = PhotoImage(file='assets/img/brothers.png')
        self.opcao_irmaos = Button(
                self.container2,
                compound=TOP,
                width=50, height=50,
                image=self.irmaos_image,
                text='Irmãos'
            )
        self.opcao_irmaos.pack(
                side=LEFT,
                padx=100,
                pady=20
            )
    
        # Menu - Documentos
        self.docs_image = PhotoImage(file='assets/img/docs.png')
        self.opcao_documentos = Button(
                self.container3,
                compound=TOP,
                width=50, height=50,
                image=self.docs_image,
                text='Documentos'
            )
        self.opcao_documentos.pack(
                side=LEFT,
                padx=100,
                pady=20
            )
    
        # Menu - Crachás
        self.cracha_image = PhotoImage(file='assets/img/badge.png')
        self.opcao_cracha = Button(
                self.container3,
                compound=TOP,
                width=50, height=50,
                image=self.cracha_image,
                text='Crachás'
            )
        self.opcao_cracha.pack(
                side=LEFT,
                padx=100,
                pady=20
            )
    
        # Menu - Serviços
        self.servicos_image = PhotoImage(file='assets/img/services.png')
        self.opcao_servicos = Button(
                self.container3,
                compound=TOP,
                width=50, height=50,
                image=self.servicos_image,
                text='Serviços'
            )
        self.opcao_servicos.pack(
                side=LEFT,
                padx=100,
                pady=20
            )

menu = Tk()
MenuGui(menu)
menu.mainloop()
